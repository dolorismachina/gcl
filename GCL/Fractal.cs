﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCL
{
    /// <summary>
    /// Class containing formulas for creating different fractal shapes.
    /// </summary>
    /// <example>
    /// <code>
    ///     Fractal f = new Fractal();
    ///     f.CreateTreeFractal(100, 500, 3, 100, 0);
    /// </code>
    /// This code creates new fractal object and fills it with tree.
    /// </example>
    public class Fractal
    {
        public Fractal()
        {
            Nodes = new List<Shape>();
        }

        /// <summary>
        /// Creates a fractal tree.
        /// </summary>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="width">Width of the node.</param>
        /// <param name="height">Height of the node.</param>
        /// <param name="rotation">Angle of the node. Positive turns it right, negative left.</param>
        public void CreateTreeFractal(float x, float y, float width, float height, float rotation)
        {
            if (height < 5 || rotation > 135 || rotation < -135)
                return;

            RectangleShape node = new RectangleShape(new Vector2f(width, height));
            node.Origin = new Vector2f(width * 0.5f, height);
            node.Position = new Vector2f(x, y);
            node.Rotation = rotation;
            if (height < 20)
                node.FillColor = new Color(0, (byte)(rnd.Next(100, 256)), 0);
            Nodes.Add(node);

            // Calculate position for next node.
            double dx = height * Math.Sin(rotation * (Math.PI / 180));
            double dy = height * Math.Cos(rotation * (Math.PI / 180));

            CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation + rnd.Next(15, 75));
            if (rnd.Next(100) > 60)
            {
                CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation);
            }
            CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation - rnd.Next(15, 75));
        }
        
        public List<Shape> Nodes { get; private set; }

        private Random rnd = new Random((int)(DateTime.Now.Ticks));
    }
}

// TODO Fix the class so that only one fractal shape can be created by the object.
// As of now the user can create different shapes using one object and they will all be contained in Nodes property resulting in all the shapes being combined.