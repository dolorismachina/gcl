﻿using System;

namespace GCL
{
    /// <summary>
    /// Collection of useful functions.
    /// </summary>
    public sealed class Utilities
    {
        /// <summary>
        /// Calculates the time difference between time of call and specified time, expressed in seconds or milliseconds.
        /// </summary>
        /// <param name="from">Moment in time from which to calculate the elapsed time.</param>
        /// <param name="milliseconds">True if time must be returned in milliseconds. False by default.</param>
        /// <returns>Time difference in seconds or milliseconds.</returns>
        public static double GetTimeDifference(DateTime from, bool milliseconds = false)
        {
            TimeSpan ts = DateTime.Now - from;

            if (milliseconds)
            {
                return ts.Ticks * Math.Pow(TimeSpan.TicksPerMillisecond, -1);
            }
            else
            {
                return ts.Ticks * Math.Pow(TimeSpan.TicksPerSecond, -1);
            }
        }
    }
}
