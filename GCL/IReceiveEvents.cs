﻿using SFML.Window;
using System;

namespace GCL
{
    /// <summary>
    /// Must be implemented if a class will be handling Mouse events.
    /// </summary>
    public interface IReceiveMouseEvents
    {
        void OnMouseMoved(MouseMoveEventArgs e);
        void OnMouseButtonPressed(MouseButtonEventArgs e);
        void OnMouseButtonReleased(MouseButtonEventArgs e);
        void OnMouseWheelMoved(MouseWheelEventArgs e);
        void OnMouseEntered(EventArgs e);
        void OnMouseLeft(EventArgs e);
    }

    /// <summary>
    /// Must be implemented if a class will be handling keyboard events.
    /// </summary>
    public interface IReceiveKeyboardEvents
    {
        void OnKeyPressed(KeyEventArgs e);
        void OnKeyReleased(KeyEventArgs e);
    }
}
