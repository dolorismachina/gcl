﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;

namespace GCL
{
    /// <summary>
    /// Main class in any game. It deals with drawing and updating the game.
    /// </summary>
    public sealed class Game : IDisposable
    {
        public Game(uint width = 800, uint height = 600, string gameTitle = "New Game", UInt16 updatesPerSecond = 20, uint antiAlasing = 2)
        {
            Window = new RenderWindow(new VideoMode(width, height, 32), gameTitle, Styles.Default, new ContextSettings(16, 0, antiAlasing));
            Window.SetVerticalSyncEnabled(false);

            millisecondsBetweenUpdate = (int)(1000 * Math.Pow(updatesPerSecond, -1));
            startTime = DateTime.Now;
            timeOfLastUpdate = DateTime.Now;

            Size = new Vector2f(width, height);
        }

        /// <summary>
        /// Starts the main loop. this is where calls to active screen's Update() and Render() are made.
        /// Update() is called whenever delta time is bigger than time in milliseconds specified by user as updateRate.
        /// Render() is called as often as possible.
        /// </summary>
        public void Run()
        {
            while (Window.IsOpen())
            {
                Render(); 
                Update(Utilities.GetTimeDifference(timeOfLastUpdate));
            }
        }

        /// <summary>
        /// Updates the logic of the currently active screen.
        /// </summary>
        /// <param name="dt">Delta time. Seconds since last call to this function.</param>
        private void Update(double dt)
        {
            if (dt * 1000 >= millisecondsBetweenUpdate)
            {
                timeOfLastUpdate = DateTime.Now;
                activeScreen.Update(dt);
            }
        }

        /// <summary>
        /// Clears the window, draws whatever activeScreen has to draw and displays it in the window.
        /// </summary>
        private void Render()
        {
            //Window.Clear(new Color(0, 100, 200));
            Window.Clear(Color.Black);
            activeScreen.Render();
            Window.Display();
        }

        /// <summary>
        /// Adds a screen to the list of available screens.
        /// </summary>
        /// <param name="screen">Screen to be registered as available screen.</param>
        public void AddScreen(Screen screen)
        {
            screens.Add(screen.Title, screen);
        }

        public void Dispose()
        {
            Window.Dispose();
        }

        // ###################################################################
        // Properties ########################################################
        // ###################################################################

        /// <summary>
        /// Size of the game screen.
        /// </summary>
        public Vector2f Size { get; set; }

        public RenderWindow Window { get; private set; }

        /// <summary>
        /// Title of the currently active screen.
        /// Although activeScreen is a Screen, the property accesses its title.
        /// This makes it possible to use any screen's title to easily change activeScreen.
        /// </summary>
        public string ActiveScreen
        {
            get
            {
                return activeScreen.Title;
            }
            set
            {
                if (screens.ContainsKey(value))
                {
                    activeScreen = screens[value];
                }
            }
        }

        public DateTime StartTime { get { return startTime; } }

        // ###################################################################
        // FIELDS ############################################################
        // ###################################################################
        private DateTime timeOfLastUpdate;
        private Screen activeScreen;
        private Dictionary<string, Screen> screens = new Dictionary<string, Screen>();
        private int millisecondsBetweenUpdate;
        readonly DateTime startTime;
    }
}